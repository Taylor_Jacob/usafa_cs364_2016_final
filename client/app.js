/**
 * Created by Colin.Schnoes on 5/10/2016.
 */
"use strict";

var app = angular.module('MTGDatabase', []);


app.controller('InventoryController', function($http) {
    var ctrl = this;
    $http.get('/users').then(function (res) {
        console.log(res.data);
        ctrl.users = res.data;
    });
    $http.get('/cards').then(function (res) {
        console.log(res.data);
        ctrl.cards = res.data;
    });
    $http.get('/decks').then(function (res) {
        console.log(res.data);
        ctrl.decks = res.data;
    });
});

app.controller('PanelController', function() {
    this.tab = 1;

    this.selectTab = function(setTab) {
        this.tab = setTab;
    };

    this.isSelected = function(checkTab) {
        return this.tab === checkTab;
    };
});

