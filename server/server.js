/**
 * Created by Warren.Watkinson on 4/13/2016.
 */
var csrf = require('csurf');
var session = require('client-sessions');
var authUtil = require('./services/authUtil');
var bodyParser = require('body-parser');
var express = require('express');
var path = require('path');
var mongoUtil = require(path.join(__dirname,'services','mongoUtil'));

// Connect to Mongo
mongoUtil.connect();

var app = express();
var routes = require(path.join(__dirname,'controllers','routes'));

app.use(express.static(path.join(__dirname,'..','client')));

// view settings
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'ejs');

// app middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
  cookieName: 'session',
  secret: 'jdkdjfhgk',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));
app.use(csrf());
app.use(authUtil.simpAuth);
app.use(routes);
// start server
var port = process.argv[2] || 3000;
app.listen(port, function() {
  var d = new Date();
  console.log(d.toUTCString() + ': Listening on port ' + port + '...');
});
