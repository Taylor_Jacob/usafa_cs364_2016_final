/**
 * Created by Warren.Watkinson on 4/15/2016.
 */

var mongo = require('mongodb');

var Cards = {
  getAll: function (callback) {
    var collection = mongo.DB.collection('cards');

    collection.find().toArray(function (err, docs) {
      callback(err, docs);
    });
  },

  findByCardName: function (cardname, callback) {
    var collection = mongo.DB.collection('cards');

    var query = {name: cardname};
    collection.find(query).limit(1).next(function (err, doc) {
      callback(err, doc);
    });

  },

  createCard: function(card,callback) {
    var collection = mongo.DB.collection('cards');

    collection.insertOne(card, function(err,result){
      //console.log('query result createCard\n' + JSON.stringify(user,null,2) + ': ' + result);
      callback(err,result);
    });
  }
};

module.exports = Cards;