/**
 * Created by C17Robert.Wlosek on 5/12/2016.
 */

var mongo = require('mongodb');

var Decks = {
    getAll: function (callback) {
        var collection = mongo.DB.collection('decks');

        collection.find().toArray(function (err, docs) {
            callback(err, docs);
        });
    },

    findByDeckName: function (deckname, callback) {
        var collection = mongo.DB.collection('decks');

        var query = {name: deckname};
        collection.find(query).limit(1).next(function (err, doc) {
            callback(err, doc);
        });

    }
};

module.exports = Decks;