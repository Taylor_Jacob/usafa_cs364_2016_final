/**
 * Created by Warren.Watkinson on 4/15/2016.
 */
var express = require('express');
var router = express.Router();
var path = require('path');

var Decks = require(path.join(__dirname,'..','models','decks'));

router
  .get("/", function(req, res) {
    Decks.getAll(function(err, docs) {
      if(err) {
        console.log('There was an error finding all decks at route /decks')
      }
      //console.log("Decks:" + JSON.stringify(docs));
      res.json(docs);
    });
  })

  .get("/:deckname", function(req, res) {
    var deckname = req.params.name;
    Decks.findByDeckName(deckname, function (err, doc) {
      if (err) {
        console.log('There was an error finding deck with deckname: ' + deckname +
          ' at route /decks/' + deckname);
      }
      //console.log("Deck:" + JSON.stringify(doc));
      res.json(doc);
    });
  });

module.exports = router;