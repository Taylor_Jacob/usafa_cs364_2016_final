var express = require('express')
var router = express.Router();
var path = require('path');
var authUtil = require('../services/authUtil');

var login = require('./login');
var Users = require('../models/users');
var cards = require(path.join(__dirname,'cards'));
var users = require(path.join(__dirname,'users'));
var decks = require(path.join(__dirname,'decks'));

var app = express();

var login = require(path.join(__dirname,'login'));
var register = require(path.join(__dirname,'register'));

router
    .use('/register', register)
    .use('/login',login)
    .use('/cards', cards)
    .use('/users', users)
    .use('/decks', decks)

    .get('/', function(req, res) {
        res.render('index');
    })

    .get('/tracker', function(req,res) {
        //res.send('hello');
        res.render('tracker', {user:req.session.user});
    })

    .get('/deck-list', authUtil.requireLogin('/login'), function(req,res) {
        //res.send('hello');
        res.render('deck-list', {user:req.session.user});
    })

    .get('/card-list', authUtil.requireLogin('/login'), function(req,res) {
        //res.send('hello');
        res.render('card-list', {user:req.session.user});
    })
    .get('/logout', function(req, res) {
        if (req.session) {
            console.log('resetting session');
            req.session.reset();
        } else {
            console.log('no session');
        }
        res.redirect('/login');
    })

    .get('/card-editor', /*authUtil.requirePermissions(['admin'],'/deck-list'),*/ function(req,res) {
          res.render('card-editor', {user:req.session.user});

    })

    .all('*',function(req,res) {
        res.redirect('deck-list');
    });

module.exports = router;
