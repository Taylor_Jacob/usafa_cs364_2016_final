/**
 * Created by Warren.Watkinson on 4/15/2016.
 */
var express = require('express');
var router = express.Router();
var path = require('path');

var Cards = require(path.join(__dirname,'..','models','cards'));

router
  .get("/", function(req, res) {
    Cards.getAll(function(err, docs) {
      if(err) {
        console.log('There was an error finding all cards at route /cards')
      }
      //console.log("Cards:" + JSON.stringify(docs));
      res.json(docs);
    });
  })

  .get("/:cardname", function(req, res) {
    var cardname = req.params.name;
    Cards.findByCardName(cardname, function (err, doc) {
      if (err) {
        console.log('There was an error finding card with cardname: ' + cardname +
          ' at route /cards/' + cardname);
      }
      //console.log("Card:" + JSON.stringify(doc));
      res.json(doc);
    });
  });

module.exports = router;