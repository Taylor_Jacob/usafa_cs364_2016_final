var express = require('express');
var authUtil = require('../services/authUtil');
var Users = require('../models/cards');

var router = express.Router();

router
  .get('/', function(req, res) {
    res.render('card-editor');
  })

  .post('/', function(req, res) {

    var card = {
      name:                req.body.card,
      cost:                req.body.cost || '',
      converted_cost:      req.body.converted_cost || '',
      type:                req.body.type,
      rarity:              req.body.rarity,
      set:                 req.body.set || '',
      effect:              req.body.effect || '',
      key_words:           req.body.key_words || '',
      power:               req.body.power || '',
      toughness:           req.body.toughness || '',
      ability:             req.body.ability || '',
      flavor_text:         req.body.flavor_text || ''
    }

    Cards.createCard(card,function(err,result) {
      if (err) {
        var error = 'Something bad happened! Please try again';

        if (err.code === 11000) {
          error = 'That card is already taken, please try another';
        }

        res.render('card-editor', {error:error});
      } else {
        res.redirect('/card-editor');
      }
    })

  });

module.exports = router;
