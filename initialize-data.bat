mongoimport --db MTG --collection cards --type json --file server/cards.json --jsonArray --drop
mongoimport --db MTG --collection users --type json --file server/users.json --jsonArray --drop
mongoimport --db MTG --collection decks --type json --file server/decks.json --jsonArray --drop
