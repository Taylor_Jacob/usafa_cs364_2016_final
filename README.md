### Project Title: MTG Database
### Team Members:
+ Colin Schnoes
+ Jacob Taylor
+ Robert Wlosek

### Description
Our project will be a database for managing and viewing Magic: The Gathering cards and decks. There will be collections for cards, decks, and users.
Users have cards and decks, where each deck has at least one card, and cards can be part of zero or many decks. 
The webpages will allow the user to view cards, view decks, create their own deck, and insert cards into their deck. The website will not be able to 
actually play a game of MTG, as the rules therein are quite complicated and would define a considerable project by itself, but it will allow the user
to create theoretical decks.
For the special feature, we will use bootstrap. The customer is MTG players who wish to create hypothetical decks.

### Installation Instructions
List here any specific installation instructions required to initialize the web application. If you followed the submission instructions it should be as simple as:

+ Requires MongoDB Server to be running
+ `npm install` to install the proper modules
+ install nodemon (however you did when I got EI. I missed it)
+ `initialize-data.bat` to resolve dependencies and to seed the MongoDB server
+ `nodemon server/server.js` to begin the application

+ Use the already existing user email of "C17Colin.Schnoes@usafa.edu" w/ password "admin"
+ To test the ability to create a new user/see only owned decks: create a user with a first name of "Jacob"

### Functionality Shortfalls
1. We weren't able to figure out why the format of the deck-list page was so odd.
2. The admin-level didn't work as the permissionsRequired function looped on itself regardless of user form.
3. All but our login forms failed, preventing the editing of decks or cards. The only CRUD functionality is the creation of new users and the returning of data.

### Specification
1. List your application requirements here
    1. All users are able to view all cards.
    2. Only admins are able to edit cards.
    3. Users are able to view their decks.
    4. Users are not able to view other users' decks.
    5. Users are able to create and edit decks.
    6. Decks are only built with existing cards.
    7. Malicious data from user is not submitted to the database
    8. Malicious data from user is not executed as a route
    9. Malicious data from user is not stored or represented on the HTML page
    10. The web pages are visually appealing.

### Development Plan
#### Sprint 1:
+ Colin Schnoes: Allow specific data to be pulled from Mongo and displayed on the web page.
+ Bobby Wlosek: Implement the design of the website and add CSS for visual appeal.
+ Jacob Taylor: Insert cards, decks, and users into the Mongo database.
+ Requirements for Sprint 1 (listed below are examples, but they should correspond to your own requirements enumeration):
    1. Requirement 1
    2. Requirement 3
    3. Requirement 10
#### Final:
+ Colin Schnoes: Implement Login and Register Pages
+ Bobby Wlosek: Design layout and CSS for Login and Register Pages
+ Jacob Taylor: Implements CRUD capability for decks and cards.

### Documentation:
#### Tracer Bullet Submission
 1. Colin Schnoes: Lab 24
 2. Bobby Wlosek: None
 3. Jacob Taylor: None

#### Design Specification Submission
 1. Colin Schnoes: None
 2. Bobby Wlosek: None
 3. Jacob Taylor: None

#### Sprint 1 Submission
 1. Colin Schnoes: Previous labs and google.
 2. Bobby Wlosek: Previous labs and google.
 3. Jacob Taylor: Previous labs and google.

#### Final Submission
 1. Colin Schnoes: Previous labs and google.
 2. Bobby Wlosek: Previous labs and google.
 3. Jacob Taylor: Previous labs and google.

### Reflections:
#### Design Specification Reflections:
+ Colin Schnoes:
  I didn't initially understand the concepts well enough to effectively design a website. We had to repeatedly redesign
  when we hit difficulties that simply didn't work well with our code at that point.
+ Bobby Wlosek:
  I thought it was interesting to be able to build an entire website with a web server and learning to make it mostly secure.
+ Jacob Taylor:
  I honestly have absolutely no idea what a tracer bullet or a sprint is. I don't understand this assignment from an
  overall perspective point of view, even if I could figure out how to do each portion of it. Therefore, I cannot possibly
  have reflections on each section, because I cannot understand the difference between the sections.

#### Tracer Bullet Reflections:
+ Colin Schnoes:
  I had no idea what I was doing, but managed to fiddle with the code from lab24 enough to get it to work. Still wasn't
  sure how it worked.
+ Bobby Wlosek:
  It is still very confusing to me, but it works and I'm good with that.
+ Jacob Taylor:
  See above.

#### Sprint 1 Reflections:
+ Colin Schnoes:
  Didn't know where to start, and neither did my team. I ended up pushing this off in favor of other schoolwork and didn't
   really do much of a Sprint 1. It was more of a do it all at once. Which isn't really working out.
+ Bobby Wlosek:
  We did not acomplish things as quickly as we should have, paid for it in the end.
+ Jacob Taylor:
  See above.

#### Final Reflections:
+ Colin Schnoes:
  I have spent more time on this project than any other project that I can recall. However, it is also the first project
  I've turned in that wasn't fully complete. It was at times both frustrating and rewarding, but always tiring. I wish I
  could have finished it, but I simply don't have the time, energy, or knowledge needed. I do thank you for you patience
  in helping me with my code. Estimated Time Spent Working: 25+ hours. Grade Deserved: This is tough, I feel like there is
  a lot to be desired with our code, but I feel like the sheer time and effort put into it over the last few days deserves
  at least a mid to low B. I've learned a lot in the last few days, and I think my grade should reflect that.  Preferably
  I'd get to keep my A in the class, but I won't be surprised if I don't.
+ Bobby Wlosek:
  Is it too late to declare management? But this was actually very interesting, but frustrating. Estimated Time Spent
  Working: 15+ hours. Grade Deserved: Low B.
+ Jacob Taylor:
  We've been rapidly approaching the point where one must decide between the desire to complete the assignment, and the
  balance of time versus potential payoff. The more time we spend getting nowhere, the lower the probability that we will
  ever actually complete the assignment (i.e., zero potential payoff for further effort input), and therefore the lower
  the chance that I will continue to have the motivation to even try. Estimated Time Spent Working: 15+ hours. Grade
  Deserved: Low B.

### Dependencies
   "bcryptjs": "^2.0.2",
   "body-parser": "^1.15.0",
   "client-sessions": "^0.7.0",
   "csurf": "^1.6.1",
   "ejs": "^2.4.1",
   "express": "^4.9.7",
   "mongodb": "^2.1.16",
   "path": "^0.12.7"
   "nodemon": "Didn't catch how you installed it."